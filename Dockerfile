# Base image
FROM python:3.5

# Base packages
RUN \
  apt-get update -qy && \
  apt-get upgrade -qy && \
  apt-get install -qy graphviz zip

# Set up Python3 packages via pip
RUN pip3 install --upgrade \
  pip \
  setuptools

# Install entrypoints "manually" to ensure a sufficiently recent version is
# available for flake8 since the version from python3-dev/pip is too old.
RUN pip3 install --upgrade --user \
  entrypoints

RUN pip3 install --upgrade \
  coverage \
  flake8

# Packages needed for hiphive
RUN pip3 install --upgrade \
  h5py \
  numba \
  numpy \
  sympy
RUN pip3 install --upgrade \
  ase \
  scikit-learn \
  scipy \
  spglib

# Packages for building documentation
RUN pip3 install --upgrade \
  sphinx_autodoc_typehints \
  sphinx-rtd-theme \
  sphinx_sitemap \
  sphinxcontrib-bibtex \
  cloud_sptheme

# Packages needed for phono3py compilation
RUN apt-get install -qy liblapack-dev liblapacke-dev libopenblas-dev

# Packages for running examples
RUN pip3 install --upgrade \
  pandas \
  phonopy==1.13.2 \
  phono3py==1.13.3.27

CMD /bin/bash
