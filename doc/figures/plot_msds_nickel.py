import numpy as np
import matplotlib.pyplot as plt

__figurefile__ = 'msds_nickel.svg'

msd_harmonic = np.loadtxt('msds_nickel/msd_harmonic.dat', unpack=True)
msd_emt = np.loadtxt('msds_nickel/msd_emt.dat', unpack=True)
msd_fcp = np.loadtxt('msds_nickel/msd_fcp.dat', unpack=True)

# plotting
plt.figure()
lw = 3.0
ms = 12.0
mew = 1.8
fs = 14

plt.plot(*msd_harmonic, '-', linewidth=lw, label='Harmonic')
plt.plot(*msd_emt, 's', markersize=ms, fillstyle='none',
         mew=mew, label='MD EMT')
plt.plot(*msd_fcp, 'o', markersize=ms, fillstyle='none',
         mew=mew, label='MD FCP')

plt.xlim([0, 1600])
plt.xlabel('Temperature (K)', fontsize=fs)
plt.ylabel(r'Mean-square displacement (Å$^2$)', fontsize=fs)
plt.gca().tick_params(labelsize=fs)

plt.legend(fontsize=fs)
plt.tight_layout()
plt.savefig(__figurefile__)
